const RandExp = require('randexp');
const mysql = require('mysql');
const fs = require('fs');
var XLSX = require('xlsx');


let con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "serprosep"
});


const patron = /^[0-9A-Fa-f]{8}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{12}$/;

const clientesRazonSocial = fs.readFileSync('./clientesRazonSocial.json');
const CRS = JSON.parse(clientesRazonSocial);

const clientes = fs.readFileSync('./clientes.json');
const CLS = JSON.parse(clientes);

const portador = fs.readFileSync('./portador.json');
const port = JSON.parse(portador);

const portadorCuip = fs.readFileSync('./portadorArmaCartuchosMod.json');
const PACMC = JSON.parse(portadorCuip);

const comisionista = fs.readFileSync('./comisionista.json');
const comis = JSON.parse(comisionista);

const armas = fs.readFileSync('./armas.json');
const arms = JSON.parse(armas);


  
var workbook = XLSX.readFile('./CP.xlsx');
var sheet_name_list = workbook.SheetNames;

con.connect(err => {
  if (err)throw err;
  for (let index = 0; index < sheet_name_list.length; index++) {
    var ordenesOriginal = XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name_list[index]])
    ordenesOriginal.forEach(data => {
        let porCuip = PACMC.find(u => u.valor2 == data.ELEMENTO);

        if(porCuip.valor1 != 'null'){
            const pass = new RandExp(patron).gen();
            let idPortador = port.find(u => u.Cuip == porCuip.valor1);
            let idCli = CLS.find(u => u.razon_social == data.RAZON_SOCIAL);
            let idArms = arms.find(u => u.matricula == porCuip.valor3);
            let idCom = comis.find(u => u.nombre == data.COMISIONISTA_RECOMENDADO);

            let total = data.PAGO_ASIGNACION + data.TRAMITES + (data.RENTA_MENSUAL * data.pagos)

            let inicio = convertirFecha(data.ENTREGA_ARMA)
            let final = convertirFechaEntrega(data.ENTREGA_ARMA, data.pagos)

            let aplicado = 0

            if(data.MARZO != ''){
                aplicado = aplicado + data.RENTA_MENSUAL
            }
            if(data.ABRIL != ''){
                aplicado = aplicado + data.RENTA_MENSUAL
            }
            if(data.MAYO != ''){
                aplicado = aplicado + data.RENTA_MENSUAL
            }
            if(data.JUNIO != ''){
                aplicado = aplicado + data.RENTA_MENSUAL
            }
            if(data.JULIO != ''){
                aplicado = aplicado + data.RENTA_MENSUAL
            }
            if(data.AGOSTO != ''){
                aplicado = aplicado + data.RENTA_MENSUAL
            }
            if(data.SEPTIEMBRE != ''){
                aplicado = aplicado + data.RENTA_MENSUAL
            }
            if(data.OCTUBRE != ''){
                aplicado = aplicado + data.RENTA_MENSUAL
            }
            if(data.NOVIEMBRE != ''){
                aplicado = aplicado + data.RENTA_MENSUAL
            }
            if(data.DICIEMBRE != ''){
                aplicado = aplicado + data.RENTA_MENSUAL
            }
            if(data.ENERO != ''){
                aplicado = aplicado + data.RENTA_MENSUAL
            }
            if(data.FEBRERO != ''){
                aplicado = aplicado + data.RENTA_MENSUAL
            }
            if(data.MARZO_23 != ''){
                aplicado = aplicado + data.RENTA_MENSUAL
            }


            let query = `INSERT INTO asignaciones(id, idCliente, id_datos_personales, id_armas, cantidad_Cartuchos, modalidad, tipo_pago, pagos, periodicidad, renta, tramite, asignacion,  total, entrega, final, tipo_movimiento, aplicado, saldo, id_comisionista, comision, activo, createddate) VALUES ('${pass}', '${idCli.id}', '${idPortador.id}', '${idArms.id}', '${porCuip.valor4}', '${porCuip.valor5}', '${data.FACTURA_EFECTIVO}', '${data.pagos}', '${data.PERIODO_PAGO}', '${data.RENTA_MENSUAL}', '${data.TRAMITES}', '${data.PAGO_ASIGNACION}', '${total}', '${inicio}', '${final}', 'ingreso', '${aplicado}', '${total}', '${idCom.id}', '1000', '${data.activo}', now())`;
            
            con.query(query, (err, result, fields) => {
                if (err) throw err;
                console.log(result)
            });


            if(data.activo != 0){
                let update = `UPDATE armas SET id_portador = '${idPortador.id}', activo = 0 where id = '${idArms.id}'`
                
                con.query(update, (err, result, fields) => {
                    if (err) throw err;
                    console.log(result)
                });
            }
            
            compromisoPagosAsignacionTramites(inicio, data.TRAMITES, data.PAGO_ASIGNACION, pass)
            compromisoPagos(inicio, data.RENTA_MENSUAL, pass, data.pagos)
            let res = mostrarcompormisoPagos(pass)

            for (let index = res.min; index < array.max; index++) {
                
                if(data.MARZO != ''){
                    asinarPago(convertirFecha(data.MARZO), data.RENTA_MENSUAL, pass, data)
                }
                if(data.ABRIL != ''){
                    asinarPago(convertirFecha(data.ABRIL), data.RENTA_MENSUAL, pass, data)
                }
                if(data.MAYO != ''){
                    asinarPago(convertirFecha(data.MAYO), data.RENTA_MENSUAL, pass, data)
                }
                if(data.JUNIO != ''){
                    asinarPago(convertirFecha(data.JUNIO), data.RENTA_MENSUAL, pass, data)
                }
                if(data.JULIO != ''){
                    asinarPago(convertirFecha(data.JULIO), data.RENTA_MENSUAL, pass, data)
                }
                if(data.AGOSTO != ''){
                    asinarPago(convertirFecha(data.AGOSTO), data.RENTA_MENSUAL, pass, data)
                }
                if(data.SEPTIEMBRE != ''){
                    asinarPago(convertirFecha(data.SEPTIEMBRE), data.RENTA_MENSUAL, pass, data)
                }
                if(data.OCTUBRE != ''){
                    asinarPago(convertirFecha(data.OCTUBRE), data.RENTA_MENSUAL, pass, data)
                }
                if(data.NOVIEMBRE != ''){
                    asinarPago(convertirFecha(data.NOVIEMBRE), data.RENTA_MENSUAL, pass, data)
                }
                if(data.DICIEMBRE != ''){
                    asinarPago(convertirFecha(data.DICIEMBRE), data.RENTA_MENSUAL, pass, data)
                }
                if(data.ENERO != ''){
                    asinarPago(convertirFecha(data.ENERO), data.RENTA_MENSUAL, pass, data)
                }
                if(data.FEBRERO != ''){
                    asinarPago(convertirFecha(data.FEBRERO), data.RENTA_MENSUAL, pass, data)
                }
                if(data.MARZO_23 != ''){
                    asinarPago(convertirFecha(data.MARZO_23), data.RENTA_MENSUAL, pass, data)
                }
                
            }

            
        }
    })
  }
});

function compromisoPagosAsignacionTramites(inicio, tramite, asignacion, pass){
    let insertTRAMITES = `INSERT INTO compromiso_pago(pago, fecha, concepto, importe, aplicado, saldo, fecha_pago, forma_pago, activo, id_asignacion,  createddate ) VALUES (1, '${inicio}', 'Tramite', '${tramite}', '${tramite}', '${inicio}', 0, 'efectivo', 0, '${pass}', now()  )`
    con.query(insertTRAMITES, (err, result, fields) => {
        if (err) throw err;
        console.log(result)
    });
    let insertASIGNACION = `INSERT INTO compromiso_pago(pago, fecha, concepto, importe, aplicado, saldo, fecha_pago, forma_pago, activo, id_asignacion,  createddate ) VALUES (1, '${inicio}', 'Asignacion', '${asignacion}', '${asignacion}', 0, '${inicio}', 'efectivo', 0, '${pass}', now() )`
    con.query(insertASIGNACION, (err, result, fields) => {
        if (err) throw err;
        console.log(result)
    });
    
}

function compromisoPagos(inicio, renta, pass, pagos){
    for (var i = 0; i < pagos; i++) {
        let insert = `INSERT INTO compromiso_pago(pago, fecha, concepto, importe, aplicado, saldo, fecha_pago, activo, id_asignacion,  createddate ) VALUES (${i+1}, '${inicio}', 'Renta', '${renta}', 0, '${renta}', '${inicio}', 1, '${pass}', now()  )`
        con.query(insert, (err, result, fields) => {
            if (err) throw err;
            console.log(result)
        });
    }
}

function mostrarcompormisoPagos(pass){
    let select = `SELECT MAX(id) max, MIN(id) min FROM compromiso_pago WHERE id_asignacion = '${pass}'`
    con.query(select, (err, result, fields) => {
        if (err) throw err;
        return result
    });
}

function asinarPago(fecha, renta, pass, id){

        let update = `UPDATE compromiso_pago SET aplicado = '${renta}', saldo = 0, fecha_pago = '${fecha}', forma_pago = 'efectivo', activo = 0 WHERE id_asignacion = '${pass}' and id = '${id}' and activo = 1`
        con.query(update, (err, result, fields) => {
            if (err) throw err;
            console.log(result)
        });
}

function convertirFecha(fecha){
    let f = fecha.split("/")
    return `${f[2]}-${f[1]}-${f[0]}`
}

function convertirFechaEntrega(fecha, type = 12){
    let f = fecha.split("/")
    switch (type) {
        case 24:
            return `${f[2]+2}-${f[1]}-${f[0]}`
        break;
        default: 
            return `${f[2]+1}-${f[1]}-${f[0]}`
    }

}